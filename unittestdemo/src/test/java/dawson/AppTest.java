package dawson;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldReturn5WhenEchoing5(){
        App application = new App();
        assertEquals("this is a test for echo method and it should return 5", 5 , application.echo(5));
    }

    @Test
    public void shouldReturn6WhenEchoing5(){
        App application = new App();
        assertEquals("this is a test for oneMore method and it should return 6 when echoing 5", 6 , application.echo(5));
    }
}
